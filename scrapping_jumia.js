const fs = require('fs');

// Import de puppeteer
const puppeteer = require("puppeteer")
const nbPage = 10
let count = 1
let data = []



const getData = async (browser, page, index) => {
    
    await page.goto('https://www.jumia.ci/smartphones/?page='+index)
    console.log("https://www.jumia.ci/smartphones/?page="+index);
    result = await page.evaluate(() =>

        [...document.querySelectorAll('section.card div article')].map(post => 
            [
                post.querySelector('a .info h3').innerText,
                post.querySelector('a .info div').innerText,
                post.querySelector('a .img-c img').getAttribute('data-src')
            ]
        ),
    )
    

    result.forEach( element =>{
        data.push(element)
    });

}

const main = async () => {
    const browser = await puppeteer.launch({ headless: true })
    const page = await browser.newPage()

    for (let index = 1; index <= nbPage; index++) {
        await getData(browser,page,index)  
    }

    browser.close()
    return data
}


main()
.then(value => {
 console.log(value)
 var myJSON = JSON.stringify(value);
 fs.writeFile('data.json', myJSON , function (err) {
    if (err) return console.log(err);
    console.log('data > data.txt');
  });
})
.catch(e => console.log(`error: ${e}`))

